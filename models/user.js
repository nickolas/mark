var mongoose   = require('mongoose')
  , Schema     = mongoose.Schema
  , timestamps = require('./plugins/timestamps')
  , S          = require('string');

User = new mongoose.Schema({
  uid: {
    type: String
  },
  name: {
    type: String
  },
  network: {
    type: String
  },
  profile: {}
});

User.plugin(timestamps);

module.exports = mongoose.model('User', User);

