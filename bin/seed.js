if (process.env.NODE_ENV === 'production') return console.log('Forbidden');

var mongoose = require('mongoose')
  , Note     = require('./../models/note')
  , maxNotes = 99;

// Establish a connection to the database
mongoose.connect((process.env.MONGOLAB_URI) ? process.env.MONGOLAB_URI : 'mongodb://localhost/mark');

// Remove all the notes then make new ones when done.
Note.remove({}, function(err) {
  if (err) {
    console.log('Error removing notes.');
    process.exit(1);
  }

  console.log('Removed all notes from database.');

  // Make a bunch of notes
  console.log('Seeding database "mark" with notes...');
  (function createNote(index) {
    if (index < 0) {
      return;
    }

    var note = new Note({
      title: 'Note ' + index,
      content: "### Title\rBelow is a bulleted list.\r\r- List item\n- List item\n- List item",
    });

    note.save(function(err, doc) {
      if (err) {
        if (index == 0) {
          process.exit(1);
        }
        return console.log(err);
      }

      console.log(doc.title + ' saved');

      if (index == 0) {
        process.exit(0);
      }
      
      return;
    });

    return createNote(index - 1);
  })(maxNotes);
});
