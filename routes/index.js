/*
 * GET home page.
 */

exports.index = function(req, res) {
  if (!req.loggedIn) {
    return res.render('login', { title: 'Mark | Login' });
  }

  res.render('index', { title: 'Mark' });
};

exports.login = function(req, res) {
  res.render('login', { title: 'Mark | Login' });
}
