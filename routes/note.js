var Note = require('./../models/note')
  , fs = require('fs')
  , exec = require('child_process').exec
  , S = require('string')
  , notAuthorized;

notAuthorized = function(res) {
  return res.send({
    "error": "Unauthorized"
  }, {
    "Content-Type": "application/json"
  }, 401);
}

exports.list = function(req, res) {
  if (!req.loggedIn) {
    return notAuthorized(res);
  }

  return Note
    .find({
      network: 'github',
      uid: req.session.auth.github.user.id
    })
    .sort('title')
    .exec(function(err, notes) {
      if (err) {
        return res.send({
          "error": err
        },{
          "Content-Type": "application/json"
        }, 500);
      }

      return res.send(notes, {
        "Content-Type": "application/json"
      }, 200);
    });
};

exports.show = function(req, res) {
  return Note
    .findOne({
      _id: req.params.id,
      network: 'github',
      uid: req.session.auth.github.user.id
    })
    .exec(function(err, note) {
      if (err) {
        return res.send({
          "error": err
        },{
          "Content-Type": "application/json"
        }, 404);
      }

      return res.send(note, {
        "Content-Type": "application/json"
      }, 200);
    });
};

exports.toHtml = function(req, res) {
  return Note
    .findOne({
      _id: req.params.id,
      network: 'github',
      uid: req.session.auth.github.user.id
    })
    .exec(function(err, note) {
      if (err || !note) {
        return res.send('<p>The document you were looking for could not be found.</p>',{
          "Content-Type": "text/html"
        }, 404);
      }

      var options = {};
      if (req.query['partial'] !== 'undefined' && req.query['partial'] == 'true') {
        options.partial = true;
      }

      return res.send(note.to_html(options),{
        "Content-Type": "text/html"
      }, 200);
    });  
};

exports.toPublicHtml = function(req, res) {
  return Note
    .findOne({
      _id: req.params.id
    })
    .exec(function(err, note) {
      if (err || !note) {
        return res.send('<p>The document you were looking for could not be found.</p>',{
          "Content-Type": "text/html"
        }, 404);
      }

      var options = {};
      if (req.query['partial'] !== 'undefined' && req.query['partial'] == 'true') {
        options.partial = true;
      }

      return res.send(note.to_html(options),{
        "Content-Type": "text/html"
      }, 200);
    });
};

exports.toText = function(req, res) {
  return Note
    .findOne({
      _id: req.params.id,
      network: 'github',
      uid: req.session.auth.github.user.id
    })
    .exec(function(err, note) {
      if (err || !note) {
        return res.send('The document you were looking for could not be found.</p>',{
          "Content-Type": "text/plain"
        }, 404);
      }

      return res.send(note.content,{
        "Content-Type": "text/plain"
      }, 200);
    });
};

exports.toPdf = function(req, res) {
  var bin  = (process.env.NODE_ENV === 'production') ? './bin/wkhtmltopdf-linux-amd64' : 'wkhtmltopdf'
    , path = 'http://'+req.headers.host+(req.url.replace('pdf', 'public'))
    , file = 'tmp/'+req.params.id+'.pdf'
    , child;

  child = exec([bin, path, file].join(' '), function(err, stdout, stderr) {
    console.log(stdout);
    console.log(stderr);
    if (err) {
      console.log('execution error: ' + err);
    }

    fs.readFile(file, function(err, content) {
      res.writeHead(200, {"Content-Type": "application/pdf"});
      res.end(content, 'utf-8');
    });
  });
};

exports.create = function(req, res) {
  var note = new Note({
    title: req.body.title,
    content: req.body.content,
    network: 'github',
    uid: req.session.auth.github.user.id
  });

  note.title = note.title.trim();
  note.content = note.content.trim();

  return note.save(function(err, note) {
    if (err) {
      return res.send({
        "error": err
      },{
        "Content-Type": "application/json"
      }, 422);
    }

    return res.send(note, {
      "Content-Type": "application/json"
    }, 200);
  });
};

exports.update = function(req, res) {
  // We do a find and update here in a single step. If any value is nil, mongo will 
  // complain that it is unable to cast the value. So we ensure that values are set
  // to the expected defaults in the case where they are blank.
  var title   = (typeof req.body.title !== 'undefined') ? req.body.title : 'untitled'
    , content = (typeof req.body.content !== 'undefined') ? req.body.content : '';

  title = S(title).trim();
  content = S(content).trim();

  return Note.findByIdAndUpdate(req.params.id, { 
      $set: {
        title: title,
        content: content
      }
    },
    function(err, note) {
      if (err) {
        return res.send({
          "error": err
        },{
          "Content-Type": "application/json"
        }, 422);
      }

      res.send(note, {
        "Content-Type": "application/json"
      }, 200);
    });
};

exports.destroy = function(req, res) {
  return Note.remove({_id: req.params.id, uid: req.session.auth.github.user.id, network: 'github'}, function(err) {
    if (err) {
      return res.send({
        "error": err
      }, {
        "Content-Type": "application/json"
      }, 404);
    }

    res.send(null, {
      "Content-Type": "application/json"
    }, 200);
  }); 
};
