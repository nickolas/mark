var express   = require('express')
  , everyAuth = require('everyauth')
  , routes    = require('./routes')
  , mongoose  = require('mongoose')
  , user      = require('./routes/user')
  , note      = require('./routes/note')
  , stylus    = require('stylus')
  , nib       = require('nib')
  , http      = require('http')
  , path      = require('path')
  , User      = require('./models/user');

// Create the app
var app = express();

// Establish a connection to the database
mongoose.connect((process.env.MONGOLAB_URI) ? process.env.MONGOLAB_URI : 'mongodb://localhost/mark');

// Create a session in mongoose.
var SessionMongoose = require("session-mongoose");
var mongooseSessionStore = new SessionMongoose({
  url: ((process.env.MONGOLAB_URI) ? process.env.MONGOLAB_URI : 'mongodb://localhost/mark'),
  interval: 120000 // expiration check worker run interval in millisec (default: 60000)
});

// Instructions for compiling stylesheets with stylus and nib.
function compile(str, path) {
  return stylus(str)
    .set('filename', path)
    .set('compress', true)
    .use(nib())
    .import('nib');
}

everyAuth.everymodule = function(userId, callback) {
  User.findById(userId, function(err, user) {
    if (err) throw err;
    if (callback) {
      callback(user);
    }
    return user;
  });
};

// Configure authentication strategies.
everyAuth.github
  .appId((process.env.MONGOLAB_URI) ? '61bde1ebe01b178f9a14' : '403e635e302eb17f8e94')
  .appSecret((process.env.MONGOLAB_URI) ? 'ce0724998fd2f52e8edf083bc999d452415090fb' : 'a4364b1f28327dd9c457fd4ba407ddf66271bc7b')
  .findOrCreateUser(function(session, accessToken, accessTokenExtra, githubUserMetadata) {
    var promise = this.Promise();

    return User.find({uid: githubUserMetadata.id, network: 'github'}, function(err, users) {
      if (err) throw err;

      if (users.length > 0) {
        return promise.fulfill(users[0]);
      } else {
        var user = new User();
        user.network = 'github';
        user.uid = githubUserMetadata.id;
        user.name = githubUserMetadata.first_name || githubUserMetadata.name;
        user.save(function(err) {
          if (err) throw err;
          return promise.fulfill(user);
        });
      }
    });

    return promise;
  })
  .redirectPath('/');

// Configure the app
app.configure(function() {
  app.set('port', process.env.PORT || 3000);
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(stylus.middleware({
    src: __dirname + '/public',
    dest: __dirname + '/public',
    compile: compile
  }));
  app.use(express.favicon());
  app.use(express.logger('dev'));
  app.use(express.bodyParser());
  app.use(express.cookieParser());
  app.use(express.session({
    store: mongooseSessionStore,
    secret: 'asdflkj24wsafjl3534341'
  }));
  app.use(everyAuth.middleware());
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(express.static(path.join(__dirname, 'public')));
});

// Add everyauth helpers.
// everyAuth.helpExpress(app); // This was deprecated.

// Configure environments
app.configure('development', function() {
  app.use(express.errorHandler());
});

// Configure routes
app.get('/', routes.index);
app.get('/users', user.list);
app.get('/notes', note.list);
app.get('/notes/:id/pdf', note.toPdf);
app.get('/notes/:id/html', note.toHtml);
app.get('/notes/:id/public', note.toPublicHtml);
app.get('/notes/:id/text', note.toText);
app.get('/notes/:id', note.show);
app.post('/notes', note.create);
app.put('/notes/:id', note.update);
app.delete('/notes/:id', note.destroy);

// Start the server
http.createServer(app).listen(app.get('port'), function(){
  console.log("Express server listening on port " + app.get('port'));
});
